const getAllCompany = (req,res,next)=>{
    console.log("Get All Companys!");
    next();
};

const getACompany = (req,res,next)=>{
    console.log("Get A Company!");
    next();
};

const postACompany = (req,res,next)=>{
    console.log("Create New Company!");
    next();
};

const putACompany = (req,res,next)=>{
    console.log("Update A Company!");
    next();
};

const deleteACompany = (req,res,next)=>{
    console.log("Delete A Company!");
    next();
};

module.exports = {
    getAllCompany,
    getACompany,
    postACompany,
    putACompany,
    deleteACompany
}