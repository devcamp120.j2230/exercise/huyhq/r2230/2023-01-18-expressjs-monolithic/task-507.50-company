const express = require("express");
const { Company } = require("../Company");
const {
    getAllCompany,
    getACompany,
    postACompany,
    putACompany,
    deleteACompany
} = require("../middlewares/CompanyMiddleware");

const companyRoute = express.Router();

var company1 = new Company("Alfreds Futterkiste", "Maria Anders", "Germany");
var company2 = new Company("Centro comercial Moctezuma", "Francisco Chang", "Mexico");
var company3 = new Company("Ernst Handel", "Roland Mendel", "Austria");

var company = {
    company1,
    company2,
    company3,
};

companyRoute.get("/",getAllCompany,(req,res)=>{
    res.status(200).json({
        message: "Get All Companys!",
        company
    })
})

companyRoute.get("/:id",getACompany,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Get Company id = "+id,
    })
})

companyRoute.post("/:id",postACompany,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Create Company id = "+id,
    })
})

companyRoute.put("/:id",putACompany,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Update Company id = "+id,
    })
})

companyRoute.delete("/:id",deleteACompany,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Delete Company id = "+id,
    })
})

module.exports = {companyRoute};